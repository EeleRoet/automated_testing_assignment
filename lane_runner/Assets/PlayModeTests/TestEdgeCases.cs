﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using UnityEngine.SceneManagement;


namespace Tests
{
    public class PlayModeTests
    {
        private Movement movementScript;
        private Score scoreScript;

        //EdgeCases: stay left, stay middle, stay right. then test distance and score.

        [UnityTest]
        public IEnumerator TestEdgecase1()
        {
            //load scene, move left, wait until death or win state.
            SceneManager.LoadScene("Assets/Scenes/SampleScene.unity");
            while (SceneManager.GetActiveScene().buildIndex != 0)
            {
                yield return null;
            }
            GameObject.Find("Player").GetComponent<Health>().SetRunningTests(true);
            GameObject.Find("WinTrigger").GetComponent<WinTriggerScript>().SetRunningTests(true);
            movementScript = GameObject.Find("Player").GetComponent<Movement>();
            scoreScript = GameObject.Find("GameManager").GetComponent<Score>();
            movementScript.MoveLeft();
            while (!movementScript.stayStill)
            {
                yield return new WaitForSeconds(1);
            }
            float achievedScore = scoreScript.GetScore();
            float achievedDistance = scoreScript.GetDistance();
            SceneManager.LoadScene("Assets/Scenes/GameOver.unity");
            //assert expected values with achieved values
            Assert.IsTrue(45.4f < achievedScore && 46f > achievedScore);
            Assert.IsTrue(35f < achievedDistance && 35.6f > achievedDistance);
            
        }

        [UnityTest]
        public IEnumerator TestEdgecase2()
        {
            //load scene, stay in the middle, wait until death or win state.
            SceneManager.LoadScene("Assets/Scenes/SampleScene.unity");
            while (SceneManager.GetActiveScene().buildIndex != 0)
            {
                yield return null;
            }
            GameObject.Find("Player").GetComponent<Health>().SetRunningTests(true);
            GameObject.Find("WinTrigger").GetComponent<WinTriggerScript>().SetRunningTests(true);
            movementScript = GameObject.Find("Player").GetComponent<Movement>();
            scoreScript = GameObject.Find("GameManager").GetComponent<Score>();
            while (!movementScript.stayStill)
            {
                yield return new WaitForSeconds(1);
            }
            float achievedScore = scoreScript.GetScore();
            float achievedDistance = scoreScript.GetDistance();
            SceneManager.LoadScene("Assets/Scenes/GameOver.unity");
            //assert expected values with achieved values
            Assert.IsTrue(81.6f < achievedScore && 82.2f > achievedScore);
            Assert.IsTrue(61.1f < achievedDistance && 61.7f > achievedDistance);
            
        }

        [UnityTest]
        public IEnumerator TestEdgecase3()
        {
            //load scene, move right, wait until death or win state.
            SceneManager.LoadScene("Assets/Scenes/SampleScene.unity");
            while (SceneManager.GetActiveScene().buildIndex != 0)
            {
                yield return null;
            }
            GameObject.Find("Player").GetComponent<Health>().SetRunningTests(true);
            GameObject.Find("WinTrigger").GetComponent<WinTriggerScript>().SetRunningTests(true);
            movementScript = GameObject.Find("Player").GetComponent<Movement>();
            scoreScript = GameObject.Find("GameManager").GetComponent<Score>();
            movementScript.MoveRight();
            while (!movementScript.stayStill)
            {
                yield return new WaitForSeconds(1);
            }
            float achievedScore = scoreScript.GetScore();
            float achievedDistance = scoreScript.GetDistance();
            SceneManager.LoadScene("Assets/Scenes/GameOver.unity");
            //assert expected values with achieved values
            Assert.IsTrue(40.9f < achievedScore && 41.5f > achievedScore);
            Assert.IsTrue(30.5f < achievedDistance && 31.1f > achievedDistance);
            
        }

        //EdgeCases: press the jump nutton every 1, 2 or 3 seconds. then test distance and score.

        [UnityTest]
        public IEnumerator TestEdgecase4()
        {
            //load scene, jump every second, wait until death or win state.
            SceneManager.LoadScene("Assets/Scenes/SampleScene.unity");
            while (SceneManager.GetActiveScene().buildIndex != 0)
            {
                yield return null;
            }
            GameObject.Find("Player").GetComponent<Health>().SetRunningTests(true);
            GameObject.Find("WinTrigger").GetComponent<WinTriggerScript>().SetRunningTests(true);
            movementScript = GameObject.Find("Player").GetComponent<Movement>();
            scoreScript = GameObject.Find("GameManager").GetComponent<Score>();
            while (!movementScript.stayStill)
            {
                movementScript.Jump();
                yield return new WaitForSeconds(1);
            }
            float achievedScore = scoreScript.GetScore();
            float achievedDistance = scoreScript.GetDistance();
            SceneManager.LoadScene("Assets/Scenes/GameOver.unity");
            //assert expected values with achieved values
            Assert.IsTrue(71.4f < achievedScore && 72f > achievedScore);
            Assert.IsTrue(61f < achievedDistance && 61.6f > achievedDistance);
            
        }

        [UnityTest]
        public IEnumerator TestEdgecase5()
        {
            //load scene, jump every second, wait until death or win state.
            SceneManager.LoadScene("Assets/Scenes/SampleScene.unity");
            while (SceneManager.GetActiveScene().buildIndex != 0)
            {
                yield return null;
            }
            GameObject.Find("Player").GetComponent<Health>().SetRunningTests(true);
            GameObject.Find("WinTrigger").GetComponent<WinTriggerScript>().SetRunningTests(true);
            movementScript = GameObject.Find("Player").GetComponent<Movement>();
            scoreScript = GameObject.Find("GameManager").GetComponent<Score>();
            while (!movementScript.stayStill)
            {
                movementScript.Jump();
                yield return new WaitForSeconds(2);
            }
            float achievedScore = scoreScript.GetScore();
            float achievedDistance = scoreScript.GetDistance();
            SceneManager.LoadScene("Assets/Scenes/GameOver.unity");
            //assert expected values with achieved values
            Assert.IsTrue(71.4f < achievedScore && 72f > achievedScore);
            Assert.IsTrue(61f < achievedDistance && 61.6f > achievedDistance);
            
        }

        [UnityTest]
        public IEnumerator TestEdgecase6()
        {
            //load scene, jump every second, wait until death or win state.
            SceneManager.LoadScene("Assets/Scenes/SampleScene.unity");
            while (SceneManager.GetActiveScene().buildIndex != 0)
            {
                yield return null;
            }
            GameObject.Find("Player").GetComponent<Health>().SetRunningTests(true);
            GameObject.Find("WinTrigger").GetComponent<WinTriggerScript>().SetRunningTests(true);
            movementScript = GameObject.Find("Player").GetComponent<Movement>();
            scoreScript = GameObject.Find("GameManager").GetComponent<Score>();
            while (!movementScript.stayStill)
            {
                movementScript.Jump();
                yield return new WaitForSeconds(3);
            }
            float achievedScore = scoreScript.GetScore();
            float achievedDistance = scoreScript.GetDistance();
            SceneManager.LoadScene("Assets/Scenes/GameOver.unity");
            //assert expected values with achieved values
            Assert.IsTrue(81.6f < achievedScore && 82.2f > achievedScore);
            Assert.IsTrue(61.1f < achievedDistance && 61.7f > achievedDistance);
            
        }


    }
}

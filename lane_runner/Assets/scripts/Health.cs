﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Health : MonoBehaviour
{
    private int lives = 3;
    private bool runningTests = false;

    public void SetRunningTests(bool newBool)
    {
        runningTests = newBool;
    }
   

    public int getLives()
    {
        return lives;
    }

    public void RemoveLive()
    {
        lives--;
        if(lives <= 0 )
        {
            if (!runningTests)
            {
                Die();
            }
            else
            {
                gameObject.GetComponent<Movement>().stayStill = true;
            }
        }
        
    }

    private void Die()
    {
        SceneManager.LoadScene("GameOver");
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RestartAfter3Sec : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(RestartAfter3Seconds());
    }

    // Update is called once per frame
    void Update()
    {
       
    }

    IEnumerator RestartAfter3Seconds()
    {

        yield return new WaitForSeconds(3f);
        SceneManager.LoadScene("SampleScene");
    }
}

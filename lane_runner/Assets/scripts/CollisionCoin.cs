﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionCoin : MonoBehaviour
{
    [SerializeField] private Score scoreScript;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.name == "Player")
        {
            scoreScript.AddScore(10f);
            scoreScript.AddCoinPickUp();
            gameObject.SetActive(false);
        }

    }
}

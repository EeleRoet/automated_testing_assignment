﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    private float score;
    [SerializeField] private Transform playerTransform;
    private float prevZ = 0;
    private float startZ;
    private float distance;
    private float coinsPickedUp;

    [SerializeField] Health healthScript;

    [SerializeField] Text scoreText;
    [SerializeField] Text lifeText;
    [SerializeField] Text pickupText;
    [SerializeField] Text distanceText;

    // Start is called before the first frame update
    void Start()
    {
        startZ = playerTransform.position.z;
    }

    // Update is called once per frame
    void Update()
    {
        AddDistanceScore();
        UpdateTexts();
    }

    public void AddScore(float scoreToAdd)
    {
        score += scoreToAdd;
        score = (float)Math.Round(score, 1);
    }

    private void AddDistanceScore()
    {
        if(prevZ == 0)
        {
            prevZ = playerTransform.position.z;
            return;
        }
        AddScore((float)Math.Round(Math.Abs(playerTransform.position.z - prevZ),1));
        prevZ = playerTransform.position.z;
        distance = (float)Math.Round( Math.Abs(prevZ - startZ),1);
    }

    public void AddCoinPickUp()
    {
        coinsPickedUp++;
    }

    private void UpdateTexts()
    {
        scoreText.text = score.ToString();
        distanceText.text = distance.ToString();
        lifeText.text = healthScript.getLives().ToString();
        pickupText.text = coinsPickedUp.ToString();
    }

    public float GetScore()
    {
        return score;
    }

    public float GetDistance()
    {
        return distance;
    }
}

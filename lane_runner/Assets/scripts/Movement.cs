﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Movement : MonoBehaviour
{
    public bool stayStill = false;
    private int currentLane = 1;
    private float moveTime = 0.3f;
    private float laneWidth = 2.5f;
    private bool moving = false;

    [SerializeField] private float jumpCooldown = 1.3f;
    private bool jumping = false;
    private float localGravity = Physics.gravity.y;

    [SerializeField] private float maxRunSpeed = 1f;
    [SerializeField] private float runAccelaration = 0.1f;

    void Update()
    {
        RunForward();
    }

    public void MoveLeft()
    {
        if(!moving && currentLane > 0)
        {
            moving = true;
            StartCoroutine(SetPlayerXSpeed(moveTime, -laneWidth));
            currentLane--;
        }
    }

    public void MoveRight()
    {
        if (!moving && currentLane < 2)
        {
            moving = true;
            StartCoroutine(SetPlayerXSpeed(moveTime, +laneWidth));
            currentLane++;
        }
    }

    public void Jump()
    {
        if (!jumping)
        {
            jumping = true;
            StartCoroutine(SetPlayerYSpeed(jumpCooldown));
        }
    }

    private void RunForward()
    {
        if(gameObject.GetComponent<Rigidbody>().velocity.z < maxRunSpeed)
        {
            gameObject.GetComponent<Rigidbody>().velocity += new Vector3(0, 0, runAccelaration);
        }
        if(stayStill)
        {
            MultiplyZ(0f);
        }
    }

    public void MultiplyZ(float factor)
    {
        Vector3 tempVector3 = gameObject.GetComponent<Rigidbody>().velocity;
        gameObject.GetComponent<Rigidbody>().velocity = Vector3.Scale(tempVector3, new Vector3(1, 1, factor));
    }

    private IEnumerator SetPlayerXSpeed(float time, float distance)
    {
        float newXSpeed = distance / time;

        gameObject.GetComponent<Rigidbody>().velocity += new Vector3(newXSpeed, 0, 0);
        yield return new WaitForSeconds(time);

        gameObject.GetComponent<Rigidbody>().velocity -= new Vector3(newXSpeed, 0, 0);
        moving = false;
        StopCoroutine(SetPlayerXSpeed(moveTime, laneWidth));
    }

    private IEnumerator SetPlayerYSpeed(float time)
    {
        float newYSpeed = localGravity * (time / 2);

        gameObject.GetComponent<Rigidbody>().velocity += new Vector3(0, -newYSpeed, 0);
        yield return new WaitForSeconds(time);

        jumping = false;
        StopCoroutine(SetPlayerYSpeed(jumpCooldown));
    }

   

}

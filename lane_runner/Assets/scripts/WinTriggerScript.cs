﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WinTriggerScript : MonoBehaviour
{
    private bool runningTests = false;
    public void SetRunningTests(bool newBool)
    {
        runningTests = newBool;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.name == "Player")
        {
            if (!runningTests)
            {
                SceneManager.LoadScene("Win");
            }
            else 
            {
                GameObject.Find("Player").GetComponent<Movement>().stayStill = true;
            }
        }

    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionScript : MonoBehaviour
{
    [SerializeField] private Health healthScript;
    [SerializeField] private Movement movementScript;
    [SerializeField] private float speedReduction;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.name == "Player")
        {
            healthScript.RemoveLive();
            movementScript.MultiplyZ(speedReduction);
            gameObject.SetActive(false);
        }
        
    }
}

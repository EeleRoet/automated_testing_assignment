﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class InputHandling : MonoBehaviour
{
    [SerializeField] Movement movementScript;
    [SerializeField] Health healthScript;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.A))
        {
            movementScript.MoveLeft();
        }
        if(Input.GetKeyDown(KeyCode.D))
        {
            movementScript.MoveRight();
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            movementScript.Jump();
        }
        if(Input.GetKeyDown(KeyCode.S))
        {
            healthScript.RemoveLive();
        }
    }
}

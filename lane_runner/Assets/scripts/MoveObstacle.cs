﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveObstacle : MonoBehaviour
{
    [SerializeField]private float frequency = 0;
    private float time;
    private float distance = 8f;
    // Start is called before the first frame update

    void Start()
    {
        time = 1 / (frequency * 2);
        StartCoroutine(SetObstacleXSpeed(time, distance));
    }

    // Update is called once per frame
    void Update()
    {
       //sadfaef 
    }

    private IEnumerator SetObstacleXSpeed(float time, float distance)
    {
        float newXSpeed = distance / time;
        gameObject.GetComponent<Rigidbody>().velocity += new Vector3(newXSpeed, 0, 0);
        yield return new WaitForSeconds(time);

        while (true)
        {
            gameObject.GetComponent<Rigidbody>().velocity -= new Vector3(2 * newXSpeed, 0, 0);
            yield return new WaitForSeconds(time);
            gameObject.GetComponent<Rigidbody>().velocity += new Vector3(2 * newXSpeed, 0, 0);
            yield return new WaitForSeconds(time);
        }
    }
}

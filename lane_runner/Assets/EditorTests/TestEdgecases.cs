﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.TestTools;

namespace Tests
{
    public class TestEdgecases
    {
      
        //EdgeCase: stay left, stay middle, stay right. then test distance and score.
        [UnityTest]
        public IEnumerator TestEdgecase1()
        {
            // Use the Assert class to test conditions.
            // Use yield to skip a frame.

            Scene playScene = EditorSceneManager.OpenScene("Assets/Scenes/SampleScene.unity");

            yield return null;
            

            Assert.IsTrue(true);

            EditorSceneManager.CloseScene(playScene, true);
           
        }

        //EdgeCase: jump every second, every 2 seconds, every 3 seconds. then test distance and score.
        [UnityTest]
        public IEnumerator TestEdgeCase2()
        {
            Scene playScene = EditorSceneManager.OpenScene("Assets/Scenes/SampleScene.unity");
            yield return null;

            Assert.IsTrue(true);

            EditorSceneManager.CloseScene(playScene, true);
        }
    }
}
